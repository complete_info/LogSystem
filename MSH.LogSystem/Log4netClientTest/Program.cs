﻿using MSH.LogClient;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Log4netClientTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //SendError();
            SendDebug();
            //SendInfo();
            //SendWarn();
            //MSHLogger.Start();
            Console.ReadKey();
        }

        private static void SendError()
        {
            Task.Run(() =>
            {
                for (int i = 0; i < 2000; i++)
                {
                    MSHLogger.Instance("测试业务1", "测试业务2").SetRequestId("123456789").Error("测试的Error日志信息");
                }
            });
        }

        private static void SendDebug()
        {
            Task.Run(() =>
            {
                for (int i = 0; i < 100; i++)
                {
                    MSHLogger.Instance("测试112业务1", "测123试业务2").Debug("测试的Err123or日志信息123");
                }
            });
        }

        private static void SendInfo()
        {
            Task.Run(() =>
            {
                for (int i = 0; i < 2; i++)
                {
                    MSHLogger.Instance("测试业务1", "测试业务2").Info("测试的Error日志信息");
                }
            });
        }

        private static void SendWarn()
        {
            Task.Run(() =>
            {
                for (int i = 0; i < 1; i++)
                {
                    MSHLogger.Instance("测试业务1", "测试业务2").SetRequestId("123456789").Warn("测试的Error日志信息");
                }
            });
        }
    }
}
